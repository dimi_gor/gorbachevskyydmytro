import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Scanner;

public class Main {

    private static List list;

    public static void main(String[] args) {

        InputStream serviceAccount = null;
        try {
            serviceAccount = new FileInputStream("tpris4-16b00-firebase-adminsdk-cnxiy-456b33a770.json");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        GoogleCredentials credentials = null;
        try {
            credentials = GoogleCredentials.fromStream(serviceAccount);
        } catch (IOException e) {
            e.printStackTrace();
        }
        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(credentials)
                .build();
        FirebaseApp.initializeApp(options);

        Firestore db = FirestoreClient.getFirestore();

        DocumentReference docRef = db.collection("5 laba").document("list");


        list = new List();

        list.init();

        System.out.println("1 - Add to the list");
        System.out.println("2 - remove from the list");
        System.out.println("3 - size of the queue");
        System.out.println("4 - show list");
        System.out.println("5 - add to firebase");
        System.out.println("6 - get from firebase");
        System.out.println("7 - exit");

        int c = 0;

        do {
            System.out.println();
            System.out.print("Your choice ");
            Scanner scanner = new Scanner(System.in);
            c = scanner.nextInt();
            switch (c) {
                case 1: {
                    System.out.print("Element:");
                    Scanner ss = new Scanner(System.in);
                    String value = ss.nextLine();
                    System.out.print("After:");
                    String after = ss.nextLine();
                    list.add(value, after);
                    break;
                }
                case 2: {
                    System.out.print("Enter element for delete :");
                    Scanner ss = new Scanner(System.in);
                    String value = ss.nextLine();
                    list.remove(value);
                    System.out.println("Deleted");
                    break;
                }
                case 3: {
                    System.out.println(list.size());
                    break;
                }
                case 4: {
                    list.show();
                    break;
                }
                case 5: {
                    list.saveToFirestore(docRef);
                    break;
                }
                case 6: {
                    list.retrieveFromFirestore(docRef);
                }
            }

        }while (c!=7);
    }
}