import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

/**
 * Created by draxvel on 18.05.2018.
 */
public class WorkWithTree{
    private static Tree tree;

    public static void main(String[] args) {


        InputStream serviceAccount = null;
        try {
            serviceAccount = new FileInputStream("tpris4-16b00-firebase-adminsdk-cnxiy-456b33a770.json");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        GoogleCredentials credentials = null;
        try {
            credentials = GoogleCredentials.fromStream(serviceAccount);
        } catch (IOException e) {
            e.printStackTrace();
        }
        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(credentials)
                .build();
        FirebaseApp.initializeApp(options);

        Firestore db = FirestoreClient.getFirestore();

        DocumentReference docRef = db.collection("5 laba").document("tree");


        tree = new Tree(new Element("TKACHUK"));


        System.out.println("1 - Input");
        System.out.println("2 - show");
        System.out.println("3 - add to firebase");
        System.out.println("4 - exit");

        int c = 0;

        do {
            System.out.println();
            System.out.print("Your choice ");
            Scanner scanner = new Scanner(System.in);
            c = scanner.nextInt();
            switch (c) {
                case 1: {
                    tree.createTree();
                    break;
                }
                case 2: {
                    tree.show();
                    break;
                }
                case 3: {
                    tree.saveToFirestore(docRef);
                    break;
                }
            }

        }while (c!=4);
    }

}
