import telebot
import config
import numpy as np
import sqlite3
import re
from firebase import firebase

firebase = firebase.FirebaseApplication('https://lab1-f2b88.firebaseio.com/', None)






OPERATORS = {
    '+': float.__add__,
    '-': float.__sub__,
    '*': float.__mul__,
    '/': float.__truediv__,
    '%': float.__mod__,
    '^': float.__pow__,
}


bot = telebot.TeleBot(config.token)

first = 0
number = 1
stek = []
nplis1 = []
nplis2 = []





def reversed_polish_notation(expr, message):


    try:
        res = eval(expr)
    except :
        bot.send_message(message.chat.id, "Вираз введено невірно")

    return res



def nullOperation(message):
    last_message = message.text
    global first
    if last_message == 'Калькулятор простих чисел з підтримкою польського запису':
        bot.send_message(message.chat.id, "Введіть вираз який необхідно обчислити")

        first = 1


    elif last_message == 'Калькулятор векторів':
        bot.send_message(message.chat.id, "Введіть вектор через пробіл")

        first = 2


    elif last_message == 'Калькулятор матриць':
        bot.send_message(message.chat.id, "Введіть матрицю")

        first = 3




    else:
        marker(message.chat.id, "Повторіть ввід типу калькулятора : ",
               ['Калькулятор простих чисел з підтримкою польського запису', 'Калькулятор векторів',
                'Калькулятор матриць'])

    return  number







def vectorCalc(message):
    global number
    if number == 1:
        firstVector(message)

    elif number == 2:
        secondVector(message)

    elif number == 3:
        resultVec(message)


def resultVec(message):
    if message.text in ['Ділення' , 'Множення', 'Додавання' , 'Віднімання', 'Скалярний добуток векторів']:
        resultVector(message)
    else:
        bot.send_message(message.chat.id, "Виберіть одну з цих дій")




def firstVector(message):
    global number
    global nplis1
    try:
        last_message = message.text
        lis = list(map(int, last_message.split()))
        nplis1 = np.array(lis)

        bot.send_message(message.chat.id, "Введіть другий вектор через пробіл")

        number = 2

    except:
        bot.send_message(message.chat.id, "Введіть ЧИСЛО")

def secondVector(message):
    global number
    global nplis2
    global nplis1
    try:
        last_message = message.text
        lis = list(map(int, last_message.split()))

        nplis2 = np.array(lis)
        if len(nplis2) != len(nplis1):
            bot.send_message(message.chat.id, "Вектори мають бути одинакових розмірів")
        else:
            number = 3

            marker(message.chat.id, 'Виберіть дію, яку потрібно виконати',
               ['Ділення', 'Множення', 'Додавання', 'Віднімання', 'Скалярний добуток векторів'])


    except:
        bot.send_message(message.chat.id, "Введіть ЧИСЛО")



def sqlread():

    result = firebase.get('/table', '1')

    return result

def resultVector(message):

    global number
    global first
    global nplis1
    global nplis2
    result = []
    print(nplis1 , nplis2)
    if message.text == 'Ділення':
        result = nplis1 / nplis2
    elif message.text == 'Множення':
        result = nplis1 * nplis2
    elif message.text == 'Додавання':
        result = nplis1 + nplis2
    elif message.text == 'Віднімання':
        result = nplis1 - nplis2
    elif message.text == 'Скалярний добуток векторів':
        result = np.dot(nplis1, nplis2)

    bot.send_message(message.chat.id, "Результат : " + str(result))


    firebase.post('/table', {'operation': str(message.text), 'oper': (str(nplis1)) + (str(nplis2)), 'result': str(result)})

    first = 0
    number = 1
    marker(message.chat.id, "Виберіть тип калькулятора : ",
           ['Калькулятор простих чисел з підтримкою польського запису', 'Калькулятор векторів',
            'Калькулятор матриць'])








def matrixCalc(message):
    global number
    if number == 1:
        firstMatrix(message)

    elif number == 2:
        secondMatrix(message)

    elif number == 3:
        resultMatrix(message)


def firstMatrix(message):
    global number
    global matrix1
    try:
        last_message = message.text
        lis = list(map(int, last_message.split()))
        matrix1 = np.array(lis)

        matrix1 = matrix1.reshape(3, 3)
        for i in range(0, matrix1.shape[0]):
            if (i % 2):
                matrix1[:, i] = 0

        bot.send_message(message.chat.id, "Введіть наступну матрицю")

        number = 2

    except:
        bot.send_message(message.chat.id, "Введіть ЧИСЛО")

def secondMatrix(message):
    global number
    global matrix2
    try:
        last_message = message.text
        lis = list(map(int, last_message.split()))
        matrix2 = np.array(lis)

        matrix2 = matrix2.reshape(3, 3)
        for i in range(0, matrix2.shape[0]):
            if (i % 2):
                matrix2[:, i] = 0


        number = 3

        marker(message.chat.id, 'Виберіть дію, яку потрібно виконати',
               ['Ділення', 'Множення', 'Додавання', 'Віднімання'])

    except:
        bot.send_message(message.chat.id, "Введіть ЧИСЛО")

def resultMatrix(message):
    global number
    global first
    global matrix2
    global matrix1
    result = []
    if message.text == 'Ділення':
        result = matrix1 / matrix2
    elif message.text == 'Множення':
        result = matrix1 * matrix2
    elif message.text == 'Додавання':
        result = matrix1 + matrix2
    elif message.text == 'Віднімання':
        result = matrix1 - matrix2




    firebase.post('/table',
                  {'operation': str(message.text), 'oper': (str(matrix1)) + (str(matrix2)), 'result': str(result)})

    bot.send_message(message.chat.id, "Результат : \n" + str(result))
    first = 0
    number = 1


    marker(message.chat.id, "Збережіть результат, або обрахуйте новий : ",
           ['Зберегти результат', 'Калькулятор простих чисел з підтримкою польського запису', 'Калькулятор векторів',
            'Калькулятор матриць'])


def marker(message_id, text, itembtn=[]):
    markup = telebot.types.ReplyKeyboardMarkup(row_width=1)
    for i in range(0, len(itembtn)):
        markup.add(itembtn[i])
    markup.one_time_keyboard = True
    bot.send_message(message_id, text, reply_markup=markup)





@bot.message_handler(commands=["start"])
def get_name_messages(message):

    global first, number



    #bot.send_message(message.chat.id, "Це 1 лабораторна робота")

    marker(message.chat.id, "Виберіть тип калькулятора  " , [ 'Калькулятор простих чисел з підтримкою польського запису' , 'Калькулятор векторів', 'Калькулятор матриць'])
    first = 0
    number = 1


@bot.message_handler(commands=["read"])
def get_read_messages(message):

    read = sqlread()
    bot.send_message(message.chat.id, str(read))
    #bot.send_message(message.chat.id, 'id - ' + str(read['id']) + '\nДані - ' + str(read['operation']) + '\nРезультат - '+ str(read['result']) )





@bot.message_handler(content_types=["text"])
def text_messages(message):

    global first, number, stek


    if first == 0:

        nullOperation(message)

    elif first == 1:
        vir = reversed_polish_notation(message.text, message)
        bot.send_message(message.chat.id, str(vir))
        firebase.post('/table',
                      {'oper': message.text, 'result': str(vir)})
        first = 0

    elif first == 2:
        vectorCalc(message)


    elif first == 3:
        matrixCalc(message)


bot.polling(none_stop=True, interval=0)