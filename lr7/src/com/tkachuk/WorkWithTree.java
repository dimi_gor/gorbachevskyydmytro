package com.tkachuk;
import java.util.Scanner;

/**
 * Created by draxvel on 18.05.2018.
 */
public class WorkWithTree {
    private static Tree tree;

    public static void main(String[] args) {

        tree = new Tree(new Element("TKACHUK"));

        System.out.println("1 - Input");
        System.out.println("2 - show");
        System.out.println("3 - delete");
        System.out.println("4 - exit");

        int c = 0;

        do {
            System.out.println();
            System.out.print("Your choice ");
            Scanner scanner = new Scanner(System.in);
            c = scanner.nextInt();
            switch (c) {
                case 1: {
                    tree.createTree();
                    break;
                }
                case 2: {
                    tree.show();
                    break;
                }
                case 3:{
                    System.out.println("Enter element for delete ");
                    Scanner scanner1=  new Scanner(System.in);
                    String s = scanner1.nextLine();
                    tree.removeNode(new Element(s));
                }
            }

        }while (c!=4);
    }

}
