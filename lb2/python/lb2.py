import time

def read(file):
    try:
        start_time = time.time()
        f = open(str(file), 'r')
        contents = f.read()
        print(contents)
        print("Час виконання - "+str(time.time() - start_time))
        f.close()
    except :
        print("File not found")



def write(file):
    start_time = time.time()
    f = open(str(file), "a+")
    fi = input("Enter text")
    print("Час виконання - "+str(time.time() - start_time))
    f.close()


def lastPos(file,string):
    try:
        start_time = time.time()
        f = open(str(file), 'r')
        contents = f.read()
        last = contents.rindex(string, 2)
        print("Останнє входження - " + str(last))
        print("Час виконання - "+str(time.time() - start_time))
        
        #stringRev = string[::-1]
        #fileRev = contents[::-1]

        
        f.close()
    except :
        print("File not found")






def main():
    while True:
        mode = input("[R]ead | [W]rite | [L]astPos | : ").upper()
        print(" ")
        if mode not in ['R', 'W', 'L']:
            print("Try again")
            continue
        file = input("Enter file name  \n")
        if mode == 'R':
            read(file)
        elif mode == 'W':
            write(file)
        elif mode == 'L':
            string = input("Enter string  \n")
            lastPos(file,string)






if __name__ == '__main__':
    main()


