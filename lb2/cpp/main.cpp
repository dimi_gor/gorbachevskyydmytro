#include <iostream>
#include <fstream>
#include <ctime>
using namespace std;

bool read(string &s, const string filename){
    ifstream myfile (filename.c_str());
    if (myfile.is_open())
    {
      if(int size = myfile.tellg()>104857600){
        cout<<"file size < 100 mb";
        return false;
      }
        getline (myfile,s);

        if(s==""){
            cout<<"empty file";
            return false;
        }

    myfile.close();
    return true;
  }
  else cout<<"file open error";
  return false;
}

void write(string s1, const string filename){
    ofstream myfile (filename.c_str());
  if (myfile.is_open())
  {
    myfile << s1;
    myfile.close();
  }
}

void copies(const string &s, string &s1, int n){
    for(int i = 0 ; i < n ; i++ ){
        s1+=s;
    }
}

int main()
{
    int start=clock();

    string s;
    string s1;

    if(read(s, "input.txt")){

            copies(s, s1, 4);

            write(s1, "output.txt");
    }
    
       
    int end = clock(); 
    cout<<(end-start)/double(CLOCKS_PER_SEC)*1000<<endl;
	system ("pause");
}

