import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

class LABA6{
    private final int N;
    private final int start;

    public LABA6(int n, int start) {
        N = n;
        this.start = start;
    }

    public int getStart() {
        return start;
    }

    public int getN() {
        return N;
    }

    public double iterative(){
        double s = 0.;

        for(int x = getStart(); x <= getN(); x++){
            s-=(((2*x)/x+1)-Math.pow(x, 2));
        }
        return s;
    }

    public double recursive(double N){
        double s = 0.;

        if (N < 1){
            return s;
        } else {
            s -= (((2*N)/N+1)-Math.pow(N, 2))-recursive(N-1);
        }

        return s;
    }

    public void addToFirestore(double iterative,
                               double recursive,
                               long timeIterative,
                               long timeRecursive) throws ExecutionException, InterruptedException {
        Firestore db = FirestoreClient.getFirestore();
        DocumentReference docRef = db.collection("LABA6").document();

        Map<String, Object> data = new HashMap<>();
        data.put("Початкове значення", getStart());
        data.put("N = ", getN());
        data.put("Результат (обчислений ітеративно)", iterative);
        data.put("Результат (обчислений рекурсивно", recursive);
        data.put("Час обчислення (ітеративно)", timeIterative);
        data.put("Час обчислення (рекурсивно)", timeRecursive);
        ApiFuture<WriteResult> result = docRef.set(data);

        System.out.println("Додано у Firebase. ("+result.get().getUpdateTime()+")");

    }
}

public class Main{

    public static void main(String... args) throws ExecutionException, InterruptedException {

        try (InputStream serviceAccount =
                     new FileInputStream("tpris4-16b00-firebase-adminsdk-cnxiy-456b33a770.json")){

            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                    .build();
            FirebaseApp.initializeApp(options);
        } catch (IOException e) {
            System.out.println("Exception:" + e.getMessage());
        }

        LABA6 t = new LABA6(1000, 1);

        System.out.println("Ітеративно: ");
        long startI = System.nanoTime();
        double iterative = t.iterative();
        long timeI = (System.nanoTime() - startI);

        System.out.println("Результат: " + iterative);
        System.out.println("Час виконання: " + timeI + " nanoseconds");

        System.out.println("\nРекурсивно: ");
        long start = System.nanoTime();
        double recursive = t.recursive(t.getN());
        long time = (System.nanoTime() - start);

        System.out.println("Результат: " + recursive);
        System.out.println("Час виконання: " + time + " nanoseconds");
        t.addToFirestore(iterative, recursive, timeI, time);
    }
}
